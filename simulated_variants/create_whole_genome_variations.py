# Import included modules
import os
import logging
import gzip
import math
from collections import defaultdict
from concurrent.futures import ProcessPoolExecutor as Pool

# Import third part modules
import daiquiri
import click
from tqdm import tqdm
from intervaltree import IntervalTree
import bgdata


# Global variables
__version__ = (0, 3, 0)
VERSION = '.'.join([str(i) for i in __version__])
CHROMOSOMES = [str(i) for i in range(1, 23)] + ['X', 'Y']
NUCLEOTIDES = 'ACGT'
BUILD = None
TREES = None
LENGTHS = None
GENOME_PATH = None
OUTPUT = None
LINES = None
FORMAT_OUTPUT = None
INCLUDE_N = None


# Set up daiquiri
daiquiri.setup(level=logging.INFO)
logger = daiquiri.getLogger('log')


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


def set_genome(genome):
    """Set the genome to use with bgreference

    :param genome: str, genome to use
    :return: None
    """

    global BUILD
    global GENOME_PATH

    if genome == 'hg19':
        from bgreference import hg19 as BUILD
    elif genome == 'hg38':
        from bgreference import hg38 as BUILD
    elif genome == 'mm10':
        from bgreference import mm10 as BUILD

    GENOME_PATH = bgdata.get_path('datasets', 'genomereference', genome)


def create_trees(annotations):
    """Create an interval tree of the annotations

    :param annotations: coordinates to use
    :return: None
    """

    if annotations is not None:
        global TREES
        global LENGTHS
        logger.info(f'Building interval tree..')
        TREES = defaultdict(IntervalTree)
        LENGTHS = defaultdict(IntervalTree)
        with gzip.open(annotations, 'rb') as fd:
            for line in fd:
                chromosome, start, end, strand, geneid, _, symbol = line.decode().strip().split('\t')
                TREES[chromosome][int(start): int(end) + 1] = None
                LENGTHS[chromosome] += int(end) - int(start) + 1


def log_number_of_files(chromosome, len_sequence=None, number_n=None):
    """Show the number of files that will be created

    :param chromosome: str, chromosome
    :param len_chromosome: str, length of the genomic sequence
    :param number_n: int, number of nucleotides N
    :return: None
    """

    if LENGTHS is not None:
        file_to_be_created =  math.ceil(LENGTHS[chromosome] * 3 / LINES)
    elif INCLUDE_N:
        file_to_be_created = math.ceil((len_sequence * 3 - number_n * 2) / LINES)
    else:
        file_to_be_created = math.ceil((len_sequence - number_n) * 3 / LINES)
    logger.info(f'Starting chromosome {chromosome}, around {file_to_be_created} files will be created...')


def parse_chromosome(chromosome):
    """Create all the possible SNVs of each chromosome

    :param chromosome: str, chromosome to parse
    :return: str, chromosome
    """

    position = 1
    num_file = 0
    num_lines = 0
    lines = []
    output = os.path.join(OUTPUT, chromosome)
    create_folder(output)

    f = os.path.join(GENOME_PATH, f'chr{chromosome}.txt')
    sequence = open(f, 'r').readline().strip().upper()
    log_number_of_files(chromosome=chromosome, len_sequence=len(sequence), number_n=sequence.count('N'))

    for position, ref in enumerate(sequence):
        genomic_position = position + 1
        if TREES is not None and not TREES[chromosome][genomic_position]:
            continue
        if ref not in NUCLEOTIDES and (not INCLUDE_N or ref != 'N'):
            continue
        alts = ['.'] if ref == 'N' else [nuc for nuc in NUCLEOTIDES if nuc != ref]
        for alt in alts:
            if FORMAT_OUTPUT == 'vep':
                l = f'{chromosome}\t{genomic_position}\t{genomic_position}\t{ref}/{alt}\t{chromosome}_{num_file}_{position}+\n'
            else:
                l = f'{chromosome}\t{genomic_position}\t{ref}\t{alt}\n'
            lines.append(l)
            num_lines += 1
            if num_lines % LINES == 0:
                fd = gzip.open(os.path.join(output, f'chr{chromosome}_{num_file}.tsv.gz'), 'wb')
                fd.write(''.join(lines).encode())
                fd.close()
                lines = []
                num_file += 1

    if len(lines) > 0:
        fd = gzip.open(os.path.join(output, f'chr{chromosome}_{num_file}.tsv.gz'), 'wb')
        fd.write(''.join(lines).encode())
        fd.close()
    logger.info(f'Chromosome {chromosome} completed!')

    return chromosome


def create_files(cpus):
    """Create the TSV file with variations

    :param cpus: number of CPUs to use
    :return: None
    """

    with Pool(max_workers=cpus) as pool:
        for _ in pool.map(parse_chromosome, CHROMOSOMES):
            pass


def create_folder(folder_path):
    """Create a folder if the folder does not exist

    :param folder_path: path of the folder to be created
    :return: None
    """

    os.makedirs(folder_path, exist_ok=True)


@click.command(context_settings=CONTEXT_SETTINGS)
@click.version_option(version=VERSION)
@click.option('-g', '--genome', type=click.Choice(['hg19', 'hg38', 'mm10']), default='hg19', help='Genome build')
@click.option('-a', '--annotations', type=click.Path(), default=None,
              help='Coordinates to use. If this option is not set the whole genome is used')
@click.option('-o', '--output', type=click.Path(), required=True, help='Output folder where to create the TSV files')
@click.option('-l', '--lines', type=click.INT, default=1000000, help='Number of lines the TSV files will have. Default is 1000000')
@click.option('--include-n/--exclude-n', default=False, required=False, help='Include or exclude (default) "N". ')
@click.option('-f', '--format-output', type=click.Choice(['tsv', 'vep']), default='tsv', help='Format of the output files')
@click.option('-c', '--cpus', type=click.IntRange(1, os.cpu_count()), default=1, help='Number of CPUs to use')
def run(genome, annotations, output, lines, include_n, format_output, cpus):
    """Create TSV files with every single variation in the genome"""

    global OUTPUT
    global LINES
    global FORMAT_OUTPUT
    global INCLUDE_N
    global CHROMOSOMES

    INCLUDE_N = include_n
    FORMAT_OUTPUT = format_output
    OUTPUT = output
    LINES = lines

    if genome.startswith('hg'):
        pass  # Uses default CHROMOSOMES
    elif genome.startswith('mm'):
        CHROMOSOMES = [str(i) for i in range(1, 20)] + ['X', 'Y']

    set_genome(genome)
    create_trees(annotations)
    create_files(cpus)


if __name__ == '__main__':
    run()
